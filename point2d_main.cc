#include "point2d.h"
#include <map>
#include <stdlib.h>

using namespace std;

int main(){
	map<string,Point> Points;
	while(true){
		string command;
		cin >> command;
		if(command=="quit")	break;
		else if(command=="set"){	//set 명령어
			string point;
			int x,y;
			cin >> point >> x >> y;
			Points.insert(make_pair(point,Point(x,y)));
		}
		else if(command=="eval"){	//eval 명령어 
			char operation;
			string lpoint,rpoint;
			cin >> lpoint >> operation >> rpoint;
			if((!isnumber(lpoint) && Points.find(lpoint)==Points.end()) || (!isnumber(rpoint) && Points.find(rpoint)==Points.end())){	//lpoint rpoint가 정의가 안되어있을 때
				cout << "input error" << endl;
			}
			else{
				Point temp;
				if(operation=='+'){	//+
					if(isnumber(lpoint) && !isnumber(rpoint))	//숫자 + Point
						temp = Point(atoi(lpoint.c_str())) + Points[rpoint];
					else if(!isnumber(lpoint) && isnumber(rpoint))	//Point + 숫자
						temp = Points[lpoint] + Point(atoi(rpoint.c_str()));
					else if(!isnumber(lpoint) && !isnumber(rpoint))	//Point + Point
						temp = Points[lpoint] + Points[rpoint];
					else if(isnumber(lpoint) && isnumber(rpoint))	//숫자 + 숫자
						temp = Point(atoi(lpoint.c_str())) + Point(atoi(rpoint.c_str()));
				}
				else if(operation=='-'){	//-
					if(isnumber(lpoint) && !isnumber(rpoint))	//숫자 - Point
						temp = Point(atoi(lpoint.c_str())) - Points[rpoint];
					else if(!isnumber(lpoint) && isnumber(rpoint))	//Point - 숫자
						temp = Points[lpoint] - Point(atoi(rpoint.c_str()));
					else if(!isnumber(lpoint) && !isnumber(rpoint))	//Point - Point
						temp = Points[lpoint] - Points[rpoint];
					else if(isnumber(lpoint) && isnumber(rpoint))	//숫자 - 숫자
						temp = Point(atoi(lpoint.c_str())) - Point(atoi(rpoint.c_str()));
				}
				else if(operation=='*'){	//*
					if(isnumber(lpoint) && !isnumber(rpoint))	//숫자 * Point
						temp = Point(atoi(lpoint.c_str())) * Points[rpoint];
					else if(!isnumber(lpoint) && isnumber(rpoint))	//Point * 숫자
						temp = Points[lpoint] * Point(atoi(rpoint.c_str()));
					else if(!isnumber(lpoint) && !isnumber(rpoint))	//Point * Point
						temp = Points[lpoint] * Points[rpoint];
					else if(isnumber(lpoint) && isnumber(rpoint))	//숫자 * 숫자
						temp = Point(atoi(lpoint.c_str())) * Point(atoi(rpoint.c_str()));
				}
				cout << "(" << temp.x_ << ", " << temp.y_ << ")" << endl;
			}
		}
	}
	return 0;
}