#include <iostream>

using namespace std;

#ifndef __hw06__point2d__
#define __hw06__point2d__

struct Point {
	int x_, y_;  // 멤버 변수.

	Point() : x_(0), y_(0){}
	Point(const Point& p) : x_(p.x_), y_(p.y_){}
	explicit Point(int c) : x_(c), y_(c){}
	Point(int x, int y) : x_(x), y_(y){}

	Point operator-() const{	// 전위 - 연산자
		return Point(-x_,-y_);
	}
};

Point operator+(const Point& lhs, const Point& rhs);
Point operator-(const Point& lhs, const Point& rhs);
Point operator*(const Point& lhs, const Point& rhs);
bool isnumber(const string& input);

#endif