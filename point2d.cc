#include "point2d.h"
#include <string>

using namespace std;

Point operator+(const Point& lhs, const Point& rhs){
	return Point(lhs.x_+rhs.x_,lhs.y_+rhs.y_);
}

Point operator-(const Point& lhs, const Point& rhs){
	return Point(lhs.x_-rhs.x_,lhs.y_-rhs.y_);
}

Point operator*(const Point& lhs, const Point& rhs){
	return Point(lhs.x_*rhs.x_,lhs.y_*rhs.y_);
}

bool isnumber(const string& input){
	for(int i=0;i<input.length();++i)
		if(input[i] < '0' || input[i] > '9')	return false;
	return true;
}