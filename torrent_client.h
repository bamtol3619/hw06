#include <iostream>
#include <vector>

#ifndef __hw06__torrent_client__
#define __hw06__torrent_client__

using namespace std;

class TorrentClient {
	public:
		TorrentClient(){}
		void add(const Torrent& torrent);
		void add_rate(const RatedTorrent& torrent);
		void delete_torrent(const string& deleting_torrent_name);
		void print();
		void rate(const string& torrent_name, const int& rating);
		void downloading(const int& second);
		// 필요한 함수 자유롭게 구현
	private:
		vector<Torrent> torrents_;  // 토렌트를 저장하는 벡터
		vector<RatedTorrent> ratedTorrents_;  // 평가된 토렌트를 저장하는 벡터
		// 그 외 필요한 변수를 추가
};

#endif