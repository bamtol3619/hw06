#include <iostream>

using namespace std;

#ifndef __hw06__parse_radix__
#define __hw06__parse_radix__

int ParseRadix(const string& number);

string ConvertRadix(int number, int radix);

#endif