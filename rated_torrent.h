#include <iostream>
#include "torrent.h"

#ifndef __hw06__rated_torrent__
#define __hw06__rated_torrent__

using namespace std;

class RatedTorrent : public Torrent {
	public:
		RatedTorrent() : Torrent(){}
		RatedTorrent(const Torrent& torrent, int rating) : Torrent(torrent), rating_(rating), download_size_(torrent.downloadSize()){}
		RatedTorrent(const string& name, int maxSize, int seeder, int rating) : Torrent(name,maxSize,seeder), rating_(rating), download_size_(0){}

		string name() const;
		string torrent_name() const	{return Torrent::name();}
		int rating() const	{return rating_;}
		int downloadSize() const	{return download_size_;}
		int download(int _second);

	private:
		int rating_;
		int download_size_;
		// 멤버변수를 정의.
};

istream& operator>>(istream& is, RatedTorrent& ratedTorrent);

#endif