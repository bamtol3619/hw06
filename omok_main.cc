#include "omok.h"

using namespace std;

int main(){
	Omok omok;
	while(true){
		int winner=NOBODY;
		int x,y;
		do{
			if(omok.Turn()==BLACK)	cout << "Black: ";	//BLACK의 턴
			else if(omok.Turn()==WHITE)	cout << "White: ";	//WHITE의 턴
			cin >> x >> y;
		}while(omok.Put(x,y)==NOBODY && cout << "Can not be placed there." << endl);
		omok.IsOmok(winner);
		if(winner!=NOBODY){
			omok.PrintMap(winner);
			break;
		}
	}
	return 0;
}
