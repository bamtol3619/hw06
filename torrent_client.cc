#include <iostream>
#include <vector>
#include "torrent.h"
#include "rated_torrent.h"
#include "torrent_client.h"

using namespace std;

void TorrentClient::add(const Torrent& torrent){
	torrents_.push_back(torrent);
}

void TorrentClient::add_rate(const RatedTorrent& torrent){
	ratedTorrents_.push_back(torrent);
}

void TorrentClient::delete_torrent(const string& deleting_torrent_name){
	bool is_delete_fail = true;
	for(vector<RatedTorrent>::iterator it = ratedTorrents_.begin(); it!=ratedTorrents_.end();++it){
		if((*it).torrent_name()==deleting_torrent_name){
			ratedTorrents_.erase(it);
			is_delete_fail = false;
			break;
		}
	}
	for(vector<Torrent>::iterator it = torrents_.begin(); it!=torrents_.end(); ++it){
		if((*it).name()==deleting_torrent_name){
			torrents_.erase(it);
			is_delete_fail = false;
			break;
		}
	}
	if(is_delete_fail)	cout << "invalid operation." << endl;
}

void TorrentClient::rate(const string& torrent_name, const int& rating){
	RatedTorrent temp;
	for(vector<Torrent>::iterator it = torrents_.begin(); it!=torrents_.end(); ++it){
		if((*it).name()==torrent_name){
			temp = RatedTorrent(*it,rating);
			ratedTorrents_.push_back(temp);
			torrents_.erase(it);
			break;
		}
	}
}

void TorrentClient::downloading(const int& second){
	for(vector<RatedTorrent>::iterator it = ratedTorrents_.begin(); it!=ratedTorrents_.end();){
		if((*it).download(second)>=(*it).maxSize())	ratedTorrents_.erase(it);
		else ++it;
	}
	for(vector<Torrent>::iterator it = torrents_.begin(); it!=torrents_.end();){
		if((*it).download(second)>=(*it).maxSize())	torrents_.erase(it);
		else ++it;
	}
}

void TorrentClient::print(){
	for(vector<RatedTorrent>::iterator it = ratedTorrents_.begin(); it!=ratedTorrents_.end();++it){
		cout << (*it).downloadSize() << "/" << (*it).maxSize() << " " << (*it).name() << endl;
	}
	for(vector<Torrent>::iterator it = torrents_.begin(); it!=torrents_.end(); ++it){
		cout << (*it).downloadSize() << "/" << (*it).maxSize() << " " << (*it).name() << endl;
	}
}