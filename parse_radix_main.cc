#include "parse_radix.h"

using namespace std;

int main(){
	while(true){
		string order;
		cin >> order;
		if(order=="quit")	break;
		else if(order=="eval"){
			string lhs,operation,rhs,result;
			int radix;
			cin >> lhs >> operation >> rhs;
			if(ParseRadix(lhs)==-1 || ParseRadix(rhs)==-1)	result="Error";	//잘못된 입력
			else if(operation=="+"){
				cin >> radix;
				result = ConvertRadix(ParseRadix(lhs)+ParseRadix(rhs),radix);
			}
			else if(operation=="-"){
				cin >> radix;
				result = ConvertRadix(ParseRadix(lhs)-ParseRadix(rhs),radix);
			}
			else if(operation=="*"){
				cin >> radix;
				result = ConvertRadix(ParseRadix(lhs)*ParseRadix(rhs),radix);
			}
			else if(operation=="/"){
				cin >> radix;
				result = ConvertRadix(ParseRadix(lhs)/ParseRadix(rhs),radix);
			}
			else if(operation=="<"){
				if(ParseRadix(lhs)<ParseRadix(rhs))	result="true";
				else	result="false";
			}
			else if(operation==">"){
				if(ParseRadix(lhs)>ParseRadix(rhs))	result="true";
				else	result="false";
			}
			else if(operation=="=="){
				if(ParseRadix(lhs)==ParseRadix(rhs))	result="true";
				else	result="false";
			}
			cout << result << endl;
		}
	}
	return 0;
}