#include <iostream>
#include "rated_torrent.h"

using namespace std;

// ‘torrent(***)' 처럼 평가 점수를 원래 이름에 추가하여 리턴 (최고점은 5점)
string RatedTorrent::name() const{
	string temp="(";
	for(int i=0;i<rating_;++i)	temp+='*';
	temp+=")";
	return Torrent::name()+temp;
}

int RatedTorrent::download(int _second){	// _second만큼 다운로드된 이후 용량 return
	download_size_+=seeder()*_second;
	return download_size_;
}

istream& operator>>(istream& is, RatedTorrent& ratedTorrent){
	string torrent_name;
	int torrent_size,torrent_seeder,torrent_rating;
	is >> torrent_name >> torrent_size >> torrent_seeder >> torrent_rating;
	if(torrent_rating>=0 && torrent_rating<=5)	ratedTorrent = RatedTorrent(torrent_name,torrent_size,torrent_seeder,torrent_rating);
}