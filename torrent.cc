#include <iostream>
#include "torrent.h"

using namespace std;

int Torrent::download(int _second){	// _second만큼 다운로드된 이후 용량 return
	download_size_+=seeder_*_second;
	return download_size_;
}


istream& operator>>(istream& is, Torrent& torrent){
	string torrent_name;
	int torrent_size,torrent_seeder;
	is >> torrent_name >> torrent_size >> torrent_seeder;
	torrent = Torrent(torrent_name,torrent_size,torrent_seeder);
}