#include <iostream>
#include <set>

using namespace std;

#ifndef __hw06__omok__
#define __hw06__omok__

#define BLACK -1
#define NOBODY 0
#define WHITE 1
#define GROUND_SIZE 19



class Omok {
	public:
		Omok() : width_(GROUND_SIZE), height_(GROUND_SIZE), turn_(BLACK) {}
		int Put(int x, int y);

// winner는 흑돌이 이긴 경우 BLACK으로, 백돌이 이긴 경우 WHITE로, 승부가 나지 않은 경우 NOBODY로 세팅.
		void IsOmok(int& winner) const;

		int Turn() const { return turn_; }

		void PrintMap(const int& winner) const;

	private:
		int width_, height_;
		int turn_;  // 마지막에 플레이한 턴을 저장
		set<pair<int, int> > blackpoint_;
		set<pair<int, int> > whitepoint_;
};

#endif
