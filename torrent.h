#include <iostream>

#ifndef __hw06__torrent__
#define __hw06__torrent__

using namespace std;

class Torrent {
	public:
		Torrent() : download_size_(0){}
		Torrent(const Torrent& Torrent) : name_(Torrent.name()), maxSize_(Torrent.maxSize()), seeder_(Torrent.seeder()), download_size_(0){}
		Torrent(const string& name, int maxSize, int seeder) : name_(name), maxSize_(maxSize), seeder_(seeder), download_size_(0){}
	
		string name() const	{return name_;}
		int seeder() const	{return seeder_;}
	
		int maxSize() const	{return maxSize_;}
		int downloadSize() const	{return download_size_;}
		int download(int _second);

	private:
		string name_;
		int maxSize_;
		int seeder_;
		int download_size_;
};

istream& operator>>(istream& is, Torrent& torrent);

#endif