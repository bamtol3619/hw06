#include "parse_radix.h"
#include <string>
#include <stdlib.h>
#include <math.h>

using namespace std;

int ParseRadix(const string& number){
	string temp;
	int radix;
	if(number.find('_')==string::npos){	//'_'가 없는 경우 10진법 사용
		temp = number;
		radix = 10;
	}
	else{
		temp = number.substr(0,number.find('_'));
		radix = atoi((number.substr(number.find('_')+1,number.length()-1)).c_str());
	}
	if(radix<=10 && radix>=2){	//2~10진법 사용
		for(int i=0;i<temp.length();++i){	//진법에 맞게 쓰인지 확인
			if(temp[i]>='0'+radix || temp[i]<'0')	return -1;
		}
	}
	else if(radix>10 && radix<=36){	//11~36진법 사용
		for(int i=0;i<temp.length();++i){	//진법에 맞게 쓰인지 확인
			if(temp[i]<'0' || (temp[i]>'9' && temp[i]<'a') || temp[i]>='a'+radix-10)	return -1;
		}
	}
	int value=0;
	for(int i=0;i<temp.length();++i){
		if(temp[i]>='0' || temp[i]<='9')	value+=(temp[i]-'0')*pow(radix,temp.length()-i-1);
		else	value+=(temp[i]-86)*pow(radix,temp.length()-i-1);
	}
	return value;
}

string ConvertRadix(int number, int radix){
	int num=number;
	bool is_positive = true;
	string answer;
	if(num<0){
		is_positive=false;
		num=-num;
	}
	while(num!=0){
		char temp;
		int r;
		r=num%radix;
		num/=radix;
		if(r<=9)	temp='0'+r;	
		else	temp=87+r;
		answer=temp+answer;
	}
	if(radix!=10){
		answer+='_';
		string radix_to_string;
		char temp;
		int r;
		while(radix!=0){
			r=radix%10;
			temp='0'+r;
			radix/=10;
			radix_to_string=temp+radix_to_string;
		}
		answer+=radix_to_string;
	}
	if(!is_positive)	answer='-'+answer;
	return answer;
}