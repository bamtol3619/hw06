#include "omok.h"
#include <string>
#include <vector>

int Omok::Put(int x, int y){
	if(x>18 || x<0 || y>18 || y<0)	return NOBODY;
	for(set<pair<int, int> >::iterator it = blackpoint_.begin(); it!=blackpoint_.end(); ++it){
		if(*it==make_pair(x,y))	return NOBODY;
	}
	for(set<pair<int, int> >::iterator it = whitepoint_.begin(); it!=whitepoint_.end(); ++it){
		if(*it==make_pair(x,y))	return NOBODY;
	}
	if(turn_==BLACK){
		blackpoint_.insert(make_pair(x,y));
		turn_=WHITE;
		return BLACK;
	}
	else if(turn_==WHITE){
		whitepoint_.insert(make_pair(x,y));
		turn_=BLACK;
		return WHITE;
	}
}

void Omok::IsOmok(int& winner) const{
	if(turn_==WHITE){	//BLACK턴에 오목인지 확인 
		for(set<pair<int,int> >::iterator it = blackpoint_.begin(); it!=blackpoint_.end(); ++it){
			int x=it->first, y=it->second;
			for(int i=1;i<5;++i){	//오른쪽위대각선으로 오목인지 확인 
				if(blackpoint_.find(make_pair(x+i,y-i))!=blackpoint_.end()){
					if(i==4){
						if(blackpoint_.find(make_pair(x-1,y+1))==blackpoint_.end() && blackpoint_.find(make_pair(x+5,y-5))==blackpoint_.end()){
							winner=BLACK;
							break;
						}
					}
					else continue;
				}
				break;
			}
			for(int i=1;i<5;++i){	//오른쪽으로 오목인지 확인 
				if(blackpoint_.find(make_pair(x+i,y))!=blackpoint_.end()){
					if(i==4){
						if(blackpoint_.find(make_pair(x-1,y))==blackpoint_.end() && blackpoint_.find(make_pair(x+5,y))==blackpoint_.end()){
							winner=BLACK;
							break;
						}
					}
					else continue;
				}
				break;
			}
			for(int i=1;i<5;++i){	//오른쪽아래대각선으로 오목인지 확인 
				if(blackpoint_.find(make_pair(x+i,y+i))!=blackpoint_.end()){
					if(i==4){
						if(blackpoint_.find(make_pair(x-1,y-1))==blackpoint_.end() && blackpoint_.find(make_pair(x+5,y+5))==blackpoint_.end()){
							winner=BLACK;
							break;
						}
					}
					else continue;
				}
				break;
			}
			for(int i=1;i<5;++i){	//아래쪽으로 오목인지 확인 
				if(blackpoint_.find(make_pair(x,y+i))!=blackpoint_.end()){
					if(i==4){
						if(blackpoint_.find(make_pair(x,y-1))==blackpoint_.end() && blackpoint_.find(make_pair(x,y+5))==blackpoint_.end()){
							winner=BLACK;
							break;
						}
					}
					else continue;
				}
				break;
			}
		}
	}
	else if(turn_==BLACK){	//WHITE턴에 오목인지 확인 
		for(set<pair<int,int> >::iterator it = whitepoint_.begin(); it!=whitepoint_.end(); ++it){
			int x=it->first, y=it->second;
			for(int i=1;i<5;++i){	//오른쪽위대각선으로 오목인지 확인 
				if(whitepoint_.find(make_pair(x+i,y-i))!=whitepoint_.end()){
					if(i==4){
						if(whitepoint_.find(make_pair(x-1,y+1))==whitepoint_.end() && whitepoint_.find(make_pair(x+5,y-5))==whitepoint_.end()){
							winner=WHITE;
							break;
						}
					}
					else continue;
				}
				break;
			}
			for(int i=1;i<5;++i){	//오른쪽으로 오목인지 확인 
				if(whitepoint_.find(make_pair(x+i,y))!=whitepoint_.end()){
					if(i==4){
						if(whitepoint_.find(make_pair(x-1,y))==whitepoint_.end() && whitepoint_.find(make_pair(x+5,y))==whitepoint_.end()){
							winner=WHITE;
							break;
						}
					}
					else continue;
				}
				break;
			}
			for(int i=1;i<5;++i){	//오른쪽아래대각선으로 오목인지 확인 
				if(whitepoint_.find(make_pair(x+i,y+i))!=whitepoint_.end()){
					if(i==4){
						if(whitepoint_.find(make_pair(x-1,y-1))==whitepoint_.end() && whitepoint_.find(make_pair(x+5,y+5))==whitepoint_.end()){
							winner=WHITE;
							break;
						}
					}
					else continue;
				}
				break;
			}
			for(int i=1;i<5;++i){	//아래쪽으로 오목인지 확인 
				if(whitepoint_.find(make_pair(x,y+i))!=whitepoint_.end()){
					if(i==4){
						if(whitepoint_.find(make_pair(x,y-1))==whitepoint_.end() && whitepoint_.find(make_pair(x,y+5))==whitepoint_.end()){
							winner=WHITE;
							break;
						}
					}
					else continue;
				}
				break;
			}
		}
	}
}

void Omok::PrintMap(const int& winner) const{
	vector<string> Map(GROUND_SIZE,"...................");
	for(set<pair<int,int> >::iterator it = blackpoint_.begin(); it!=blackpoint_.end(); ++it)	Map[it->second][it->first]='o';
	for(set<pair<int,int> >::iterator it = whitepoint_.begin(); it!=whitepoint_.end(); ++it)	Map[it->second][it->first]='x';
	for(int i=0;i<GROUND_SIZE;++i){
		for(int j=0;j<GROUND_SIZE;++j){
			cout << Map[i][j] << " ";
		}
		cout << endl;
	}
	if(winner==BLACK)	cout << "Winner: Black player" << endl;
	else if(winner==WHITE)	cout << "Winner: White player" << endl;
}
