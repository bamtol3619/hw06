#include <iostream>
#include "torrent.h"
#include "rated_torrent.h"
#include "torrent_client.h"

using namespace std; 

int main(){
	TorrentClient Torrents;
	while(true){
		string order;
		cin >> order;
		if(order=="quit")	break;
		
		else if(order=="add"){
			Torrent temp;
			cin >> temp;
			Torrents.add(temp);			
		}
		
		else if(order=="add_rate"){
			RatedTorrent temp;
			cin >> temp;
			if(temp.rating()>=0 && temp.rating()<=5)	Torrents.add_rate(temp);
		}
		
		else if(order=="delete"){
			string deleting_torrent_name;
			cin >> deleting_torrent_name;
			Torrents.delete_torrent(deleting_torrent_name);			
		}
		
		else if(order=="print"){
			Torrents.print();
		}
		
		else if(order=="download"){
			int second;
			cin >> second;
			Torrents.downloading(second);			
		}
		
		else if(order=="rate"){
			string rate_torrent_name;
			int rate_rating;
			cin >> rate_torrent_name >> rate_rating;
			if(rate_rating>=0 && rate_rating<=5){
				Torrents.rate(rate_torrent_name, rate_rating);
			}
		}
		
		else	cout << "invalid operation." << endl;
	}
	return 0;
}